// $Id: README.txt,v 1.0 2013/11/13 17:22:02 acaster

-- REQUIREMENTS --

DownloadFile

-- INSTALLATION --
  
1) Copy the download_image folder to the modules folder in your installation.

2) Enable the module using Administration -> Modules (/admin/modules).
  
3) Manage teaser and full node display settings at Administration -> Structure 
   -> Content types -> "your type" -> manage display 
   (admin/structure/types/manage/"your type"/display).
  
4) Choose a formatter to apply to files or images in that field. Four new 
   formatters "Image download formatter" appear in the select list.
